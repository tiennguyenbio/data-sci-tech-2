# Project Title

A brief description of your project.

## Table of Contents

- [Introduction](#introduction)
- [Business Scenario](#business-scenario)
- [Dataset](#dataset)
- [Problem Formulation and Data Collection](#problem-formulation-and-data-collection)
- [Setup](#setup)
- [Data Preprocessing and Visualization](#data-preprocessing-and-visualization)
- [Combining CSV Files](#combining-csv-files)
- [Model Training and Evaluation](#model-training-and-evaluation)
- [Features](#features)
- [Contributing](#contributing)
- [License](#license)

## Introduction

The goals of this notebook are:
- Process and create a dataset from downloaded ZIP files
- Exploratory data analysis (EDA)
- Establish a baseline model and improve it

## Business Scenario

You work for a travel booking website that is working to improve the customer experience for flights that were delayed. The company wants to create a feature to let customers know if the flight will be delayed due to weather when the customers are booking the flight to or from the busiest airports for domestic travel in the US. 

You are tasked with solving part of this problem by leveraging machine learning to identify whether the flight will be delayed due to weather. You have been given access to the a dataset of on-time performance of domestic flights operated by large air carriers. You can use this data to train a machine learning model to predict if the flight is going to be delayed for the busiest airports.

## Dataset

The provided dataset contains scheduled and actual departure and arrival times reported by certified US air carriers that account for at least 1 percent of domestic scheduled passenger revenues. The data was collected by the Office of Airline Information, Bureau of Transportation Statistics (BTS). The dataset contains date, time, origin, destination, airline, distance, and delay status of flights for flights between 2014 and 2018.
The data are in 60 compressed files, where each file contains a CSV for the flight details in a month for the five years (from 2014 - 2018). The data can be downloaded from this link: [https://ucstaff-my.sharepoint.com/:f:/g/personal/ibrahim_radwan_canberra_edu_au/Er0nVreXmihEmtMz5qC5kVIB81-ugSusExPYdcyQTglfLg?e=bNO312]. Please download the data files and place them on a relative path. Dataset(s) used in this assignment were compiled by the Office of Airline Information, Bureau of Transportation Statistics (BTS), Airline On-Time Performance Data, available with the following link: [https://www.transtats.bts.gov/Fields.asp?gnoyr_VQ=FGJ]. 

## Problem Formulation and Data Collection

### 1. Determine if and why ML is an appropriate solution to deploy.

The business goal of the travel booking website is to improve customer experience while booking the domestic flight to or from the busiest airport in the US by predicting delayed flights due to weather. 

The selected data will be records of every flights during the period of five years (2014 - 2018) that could be considered as a moderately large dataset. The business problem might involves different variables such as duration of flight, arrival and departure time, arrival and departure airport, flight distance, airplanes, daily weather data, ect. These variable might have complex patterns that are difficult to be analysed by using traditional statistical methods. Therefore, it's necessary to have a powerful tool to digest the available data, recognize the complex pattern, make accurate predictions. In addition, the flight and weather parameters are continue changing, which require a model that can update the change. 

Machine learning is an appropriate solution that satify the above requirements. Using machine learning to predict flight delays due to weather will benefits the enterprise and their customers by providing robust data processing tools to work with large dataset, efficient predicting algorithms, and high adaptibility to the changing of data.

### 2. Formulate the business problem, success metrics, and desired ML output.

The main business problem is to predict domestic flight delays due to weather across top busiest airports in the USA. This will help to reduce the costs of fight delays on customers and airline companies. With the flight delays forecast, the airline companies can optimize their operation to improve customer satisfaction, thus the passengers could be well-prepared for their travels.

As a classification task, success metrics to be measured for the machine learning model's performance includes accuracy, precision, recall, specificity, area under the curve (AUC).

The prediction of delayed flights is categorised as binary classification task where the desired machine learning outputs are "0" for "no delayed" and "1" for "delayed". The expectation of this project is to understand the data, develop a ML model to predict a fight is delay or not based on known information, visualize the predicted results, evaluate the performance and deploy the model.

### 3. Identify the type of ML problem you’re dealing with.

The type of machine learning problem we are dealing with in this scenario is a binary classification problem. We aim to classify flights into two categories: those that will be delayed due to weather (class "1") and those that will not be delayed (class "0"). The model will make predictions based on historical data, including flight schedules, airline information, and weather conditions. Additionally, time series analysis may be applied to capture temporal aspects of weather conditions.

## Setup

The libraries used in this project to prepare data and build a logistic regression model for binary classification, which includes: os, pathlib2, zipfile, time, pandas, numpy, subprocess, matplotlib, seaborn, sklearn.model_selection, sklearn.metrics, sklearn.linear_model

## Combining CSV Files

60 csv files were extracted from 50 zip files. 
Each csv file was converted into a dataframe, then subset only the required data before merging into one file.

## Data Preprocessing and Visualization



## Model Training and Evaluation

Data was splited into training (80%) and testing (20%)
A classifier = Logistic.Regression() was used to build a classification model


## Features

We consider the following columns to predict an arrival delay:<br>

`Year`, `Quarter`, `Month`, `DayofMonth`, `DayOfWeek`, `FlightDate`, `Reporting_Airline`, `Origin`, `OriginState`, `Dest`, `DestState`, `CRSDepTime`, `DepDelayMinutes`, `DepartureDelayGroups`, `Cancelled`, `Diverted`, `Distance`, `DistanceGroup`, `ArrDelay`, `ArrDelayMinutes`, `ArrDel15`, `AirTime`

You will also filter the source and destination airports to be:
- Top airports: ATL, ORD, DFW, DEN, CLT, LAX, IAH, PHX, SFO
- Top 5 airlines: UA, OO, WN, AA, DL

The hour of the day in 24-hour time format was extracted from 'CRSDepTime', then convert into four buckets: night, moring, afternoon, evening

ArrDel15 was chosen as target for the classification task

One Hot encoding was applied to all categorical variables

## Contributing

This project is not available for contributing

## License

No License applied
